import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import App from './containers/App'

import './styles/index.scss';

import store from './store';

render(
	<Provider store={store}>
		<App store={store}/>
	</Provider>,
	document.getElementById('app')
);

if(module.hot) {
	module.hot.accept();
}
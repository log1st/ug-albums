import {
	SAVE_ALBUM, REMOVE_ALBUM
} from "../actions";

const initialState = {
	albums: JSON.parse(localStorage.getItem('albums')) || []
};

export default function friends(state = initialState, action) {
	let index;
	switch (action.type) {
		
		case SAVE_ALBUM:
			index = state.albums.indexOf(action.id);
			
			if(index === -1) {
				state.albums.push(action.id);
			}
			
			return {
				albums: state.albums
			};
			
		case REMOVE_ALBUM:
			index = state.albums.indexOf(action.id);
			
			state.albums.splice(index, 1);
			
			return {
				albums: state.albums
			};
		
		default:
			return state;
	}
}
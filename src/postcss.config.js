module.exports = {
	parser: 'postcss-scss',
	plugins: {
		'postcss-import': {},
		'cssnext': {},
		'autoprefixer': {
			browsers: ['last 2 versions', '> 5%']
		},
		/*
		'cssnano': {
			zindex: false,
			reduceIdents: false
		}
		*/
	}
};
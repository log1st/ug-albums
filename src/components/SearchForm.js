import React, {Component} from 'react'

import Album from './Album.jsx';

export default class SearchForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
			query: '',
			matches: [],
			timeout: null,
			isLoading: false
		};
		
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.sendQuery = this.sendQuery.bind(this);
	}
	
	handleChange(event) {
		this.setState({query: event.target.value});
		
		if (event.target.value.length < 3) {
			return;
		}
		
		if (this.timeout) {
			clearTimeout(this.timeout);
		}
		
		this.timeout = setTimeout(this.sendQuery, 250);
	}
	
	handleSubmit(event) {
		if (this.timeout) {
			clearTimeout(this.timeout);
		}
		event.preventDefault();
		
		this.sendQuery();
	}
	
	sendQuery() {
		if (this.state.isLoading) {
			return;
		}
		
		this.setState({
			isLoading: true
		});
		
		let xhr = new XMLHttpRequest();
		
		xhr.open('GET', 'http://musicbrainz.org/ws/2/release?query=' + this.state.query + '&limit=10', true);
		
		xhr.setRequestHeader('Accept', 'application/json');
		
		xhr.addEventListener('readystatechange', () => {
			if(xhr.readyState === 4) {
				let response = JSON.parse(xhr.responseText);
				
				this.setState({
					isLoading: false,
					matches: response['releases'].map(release => {
						return {
							id: release['id'],
							title: release['title'],
							artists: release['artist-credit'],
							date: release['date'],
							trackCount: release['track-count'],
						}
					})
				})
			}
		});
		
		xhr.send();
	}
	
	render() {
		return (
			<form onSubmit={this.handleSubmit}>
				<input
					placeholder={'Type 3 symbols to start search'}
					type="text"
					onInput={this.handleChange}
				/>
				
				<p>{this.state.isLoading ? 'Loading...' : ''}</p>
				
				<div className={'albums'}>
					{this.state.matches.map(album =>
						<div key={album.id} className={'albums__album'}>
							<Album store={this.props.store} {...album}/>
						</div>
					)}
				</div>
			</form>
		);
	}
}
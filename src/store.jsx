import {createStore} from "redux";
import rootReducer from "./reducers/index";

const store = createStore(rootReducer);

store.subscribe(() => {
	localStorage.setItem('albums', JSON.stringify(store.getState().albums));
});

export default store;
import React, {Component} from 'react'

import SearchForm from '../components/SearchForm';

export default class App extends Component {
	render() {
		return <div>
			<h1>UG Albums</h1>
			<SearchForm store={this.props.store}/>
		</div>
	}
}
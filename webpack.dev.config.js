'use strict';

const
	path = require('path'),
	srcDir = path.resolve(__dirname, 'src'),
	distDir = path.resolve(__dirname, 'dist'),
	host = '127.0.0.1',
	port = 8080,
	publicPath = 'http://' + host + ':' + port + '/';

const
	webpack = require('webpack'),
	HTMLWebpackPlugin = require('html-webpack-plugin'),
	HTMLWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');


module.exports = {
	mode: 'development',
	context: srcDir,
	entry: {
		main: [
			'babel-polyfill', './index.jsx',
		]
	},
	output: {
		library: '[name]',
		path: distDir,
		filename: '[name].js',
		publicPath: publicPath
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	module: {
		rules: [
			{
				test: '/\.js$/',
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['env']
				}
			},
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {
					presets: ['react', 'env']
				}
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: "style-loader"
					},
					{
						loader: "css-loader",
						options: {
							minimize: false,
							sourceMap: true
						}
					}, {
						loader: 'postcss-loader',
						options: {
							sourceMap: true
						}
					}, {
						loader: "sass-loader",
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(jpg|jpeg|png|gif|ico|mp4)$/,
				loader: 'file-loader',
				options: {
					filename: '[name].[hash].[ext]',
				}
			}
		]
	},
	plugins: [
		new HTMLWebpackPlugin({
			filename: path.resolve(distDir, 'index.html'),
			xhtml: true,
			template: path.resolve(srcDir, 'index.html'),
			alwaysWriteToDisk: true
		}),
		new HTMLWebpackHarddiskPlugin,
		new webpack.HotModuleReplacementPlugin,
		new webpack.DefinePlugin({
			DEBUG: true
		})
	],
	devtool: 'inline-source-map',
	watch: true,
	devServer: {
		port: port,
		hot: true,
		host: host,
		contentBase: distDir,
		watchOptions: {
			poll: true
		},
		headers: {
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Headers': '*'
		}
	}
};
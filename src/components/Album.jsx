import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {saveAlbum, removeAlbum} from "../actions";

class Album extends Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			isAdded : this.props.store.getState().albums.indexOf(this.props.id) > -1
		};
		
		console.log(this.props.id, this.state.isAdded);
		
		this.handleClick = this.handleClick.bind(this);
	}
	
	handleClick(e) {
		e.preventDefault();
		
		if(this.isAdded()) {
			this.deleteFromStore();
		}   else {
			this.saveToStore();
		}
	}
	
	isAdded() {
		return this.state.isAdded;
	}
	
	saveToStore() {
		this.props.store.dispatch(saveAlbum(this.props.id));
		this.setState({
			isAdded: true
		})
	}
	
	deleteFromStore() {
		this.props.store.dispatch(removeAlbum(this.props.id));
		this.setState({
			isAdded: false
		})
	}
	
	render() {
		return <div className={'album'}>
			<b className="album__title">{this.props.title}</b>
			{
				(this.props.date) ?
				<b className="album__date">{this.props.date}</b> : ''
			}
			<b className="album__tracksCount">{this.props.trackCount}</b>
			<div className="album__artists">
				<dl className="album__artist">
					<dt>Artist</dt>
					<dd>{this.props.artists.map(item => <span key={item.artist.id}>
					{item.artist.name}
				</span>)}</dd>
				</dl>
			</div>
			<button className={`album__button ${this.isAdded() ? 'album__button--remove' : 'album__button--add'}`} onClick={this.handleClick}>
				<b>{!this.isAdded() ? 'Add to favourites' : 'Remove from favourites'}</b>
			</button>
			<div className="album__id">{this.props.id}</div>
		</div>
	}
	
}

Album.propTypes = {
	id: PropTypes.string,
	title: PropTypes.string,
	artists: PropTypes.array,
	date: PropTypes.string,
	trackCount: PropTypes.number,
};

export default connect()(Album);
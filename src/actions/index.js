export const SAVE_ALBUM = 'SAVE_ALBUM';
export const REMOVE_ALBUM = 'REMOVE_ALBUM';

export const saveAlbum = id => ({
	type: SAVE_ALBUM,
	id
});

export const removeAlbum = id => ({
	type: REMOVE_ALBUM,
	id
});